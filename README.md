# Search Test Application

A simple application which search given string against an array of Json and return result based on given levenshtein distance threshold

## Setup

Clone the repo and install the dependencies.

```bash
git clone https://gitlab.com/ateamindia/demo-projects/levenshtein-search.git
cd levenshtein-search
```

```bash
npm install
```

To start the server, run the following

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) and take a look around.
