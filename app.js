const express = require('express');
const app = express();
const port = 3000;
const fs = require('fs').promises;
const { findWordMatch, getDistance } = require('./search');

// set the view engine to ejs
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('index');
});

/**
 * GET /search
 * @summary Function to handle the search logic
 * @return {Array} List of entity
 */
app.get('/search', async (req, res) => {
    const searchTerm = req.query.term;
    const maxDistance = req.query.maxDistance || 1;
    const reportsFileContent = JSON.parse(await fs.readFile('reportsWithKeywords.json', 'utf8'));
    let response = [];
    if (reportsFileContent) {
        for (const entity of reportsFileContent.reports) {
            if (findWordMatch(searchTerm, entity.name, maxDistance)) {
                response.push(entity);
            } else if (findWordMatch(searchTerm, entity.category, maxDistance)) {
                response.push(entity);
            } else {
                for (const keyword of entity.keywords) {
                    if (findWordMatch(searchTerm, keyword, maxDistance)) {
                        response.push(entity);
                        break;
                    }
                }
            }
        }
    }
    // Find the levenshtein distance of each word to search term
    response = response.map((entity) => {
        const nameDistance = getDistance(searchTerm, entity.name);
        const categoryDistance = getDistance(searchTerm, entity.category);
        const keywordsDistance = entity.keywords.map((keyword) => getDistance(searchTerm, keyword));

        return {
            code: entity.code,
            name: entity.name,
            nameDistance,
            categoryDistance,
            keywordsDistance,
        };
    });
    res.send(response);
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
